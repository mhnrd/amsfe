import { getStatusBarHeight } from 'react-native-status-bar-height';

export const DefaultInputTopSpacing = {

    marginTop:12

} 

export const DefaultButtonSize = {

    marginTop:17,
    width: '100%',
    backgroundColor:'#000080'

}

export const TopNavigation = {

    backgroundColor: 'transparent',
    paddingTop: getStatusBarHeight() + 12,

};

export const DefaultThemeColor = {

    color:'black'

}