import AsyncStorage from '@react-native-community/async-storage';
import Strings from '../constants/Strings';


const addToCart = async (item: any) => {

    let stringCart = JSON.stringify(item);
    AsyncStorage.setItem(Strings.KeyStorage.Cart, stringCart);

}

export {
    addToCart
}