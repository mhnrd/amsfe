import AsyncStorage from '@react-native-community/async-storage';
import Strings from '../constants/Strings';

const setUserAuthorizationToken = async (token: string) => {
    try {
      await AsyncStorage.setItem(Strings.KeyStorage.Authorization, token);
    } catch (e) {
      console.log('FAILED TO SAVE TOKEN @Sessions: ' + e);
    }
};

const setUserAccountData = async (account = {}) => {
    try {
        if (Object.keys(account).length > 0) {
          await AsyncStorage.setItem(
            Strings.KeyStorage.AccountData,
            JSON.stringify(account)
          );
        }
      } catch (e) {
        console.log('FAILED TO SAVE TOKEN @Account Data: ' + e);
    }
}

const setUserInformation = async (user = {}) => {
  try {
    await AsyncStorage.setItem(Strings.KeyStorage.UserInformation, JSON.stringify(user));
  } catch (e) {
    console.log('FAILED TO SAVE TOKEN @User Information: ' + e);
  }
};

const clearSessions = async () =>{
    await AsyncStorage.removeItem(Strings.KeyStorage.Authorization);
    await AsyncStorage.removeItem(Strings.KeyStorage.AccountData);
    await AsyncStorage.removeItem(Strings.KeyStorage.UserInformation);
}

export {
    setUserAuthorizationToken,
    setUserAccountData,
    setUserInformation,
    clearSessions
}