import Constants from 'expo-constants';

// Change based on your network IP.
// ip config for Windows OS cmd, ifconfig for macOS terminal
const PORT = '2001';
const IP_ADDRESS = 'http://192.168.1.3';
//'https://single-api.primeambrella.com/', 
const API = {
  dev: 'https://single-api.primeambrella.com/',//`${IP_ADDRESS}:${PORT}/`, //'https://single-api.primeambrella.com/', 
  staging:
    'https://single-api.primeambrella.com/',
  production:
    'https://single-api.primeambrella.com/',
};

export function getReleaseChannel(releaseChannel = 'dev') {
  if (releaseChannel.indexOf('production') !== -1) {
    return 'production';
  }
  if (releaseChannel.indexOf('staging') !== -1) {
    return 'staging';
  }
  return 'dev';
}

function getEnvVars(env = 'dev') {
  if (env.indexOf('staging') !== -1) {
    return API.staging;
  }
  if (env.indexOf('production') !== -1) {
    return API.production;
  }
  return API.dev;
}

const environment = {
  baseUrl: getEnvVars(Constants.manifest.releaseChannel),
};

const releaseChannel = getReleaseChannel(Constants.manifest.releaseChannel);

export { releaseChannel, getEnvVars };
export default environment;
