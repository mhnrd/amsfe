
  
  const KeyStorage = {
    Authorization: 'Authorization',
    AccountData: 'AccountData',
    Cart: 'Cart',
    UserInformation: 'UserInformation',
  };
  
  export default {
    KeyStorage,
  };
  