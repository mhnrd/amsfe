export enum Screens {
    LoginScreen = 'LoginScreen',
    HomeScreen = 'HomeScreen',
    ProfileScreen = 'ProfileScreen',
    OrderScreen = 'OrderScreen',
    NotificationScreen = 'NotificationScreen',
    ShoppingCartScreen = 'ShoppingCartScreen',
    SettingScreen = 'SettingScreen',
    PasswordScreen = 'PasswordScreen',
    AddressScreen = 'AddressScreen',
    ProductDetailScreen = 'ProductDetailScreen',
}