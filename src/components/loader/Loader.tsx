import React from  'react';
import {
    Modal,
    Text,
    View,
  } 
from "react-native";
import {
    SkypeIndicator,
} from "../../lib/loader";
import Styles from './Style';

const Loader = (props: any) => {
  
    return (
    <>
        <Modal
            animationType="fade"
            transparent={true}
            visible={ props.visible }
            >
            <View style={Styles.centeredView}>
            <View style={Styles.modalView}>
                
                { props.type == 'loader' ? (
                    <SkypeIndicator color={ props.color } />
                ) : (<Text>Icon here</Text>) }
                
                <Text style={Styles.modalText}>{ props.title }</Text>            
            </View>
            </View>
        </Modal>
    </>
    )
}

export default Loader;