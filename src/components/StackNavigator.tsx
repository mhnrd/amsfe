import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

//Function components
import { LoginScreen } from './../screens/LoginScreen/LoginScreen';
import { HomeScreen } from './../screens/HomeScreen/HomeScreen';


const RoutedApp = createAppContainer(
   
    createStackNavigator({
      LoginScreen: { screen: LoginScreen },
      HomeScreen: { screen: HomeScreen },
    
      /*OTPScreen: {
        screen: OTPScreen,
        navigationOptions: {
          gestureEnabled: false,
        },
      },*/
     
      },
      {
        headerMode: 'none',
        initialRouteName: 'LoginScreen',
        navigationOptions: {
          gesturesEnabled: false,
        },
    })

);

export { RoutedApp }