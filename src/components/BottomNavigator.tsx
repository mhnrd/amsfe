import React, { useState } from  'react';
import {
    BottomNavigation, 
    BottomNavigationTab, 
    Icon
  } from '@ui-kitten/components';
import {
    View,
    Image,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';
import { Badge } from 'react-native-elements'
import { Screens } from '../constants/Enum';
import { useNavigation, useNavigationParam } from 'react-navigation-hooks'
import { TouchableOpacity } from 'react-native-gesture-handler';

const PersonIcon = (props: any) => (
    <Icon {...props} name='log-out'/>
);

const HomeIcon = (props: any) => (
    <Icon {...props} name='home'/>
);

const BoxIcon = (props: any) => (
    <TouchableWithoutFeedback onPress={() => {alert('test scanned')} }>
        <View
            style={{
            position: 'absolute',
            bottom: 0, // space from bottombar
            height: 68,
            width: 68,
            borderRadius: 58,
            backgroundColor: '#5a95ff',
            justifyContent: 'center',
            alignItems: 'center',
            }}>
            <Image
            source={require('../images/scan.png')}
            style={{
                width: 40,
                height: 40,
                tintColor: '#f1f6f9',
                alignContent: 'center',
            }}
            />
        </View>
    </TouchableWithoutFeedback>
    
);

const useBottomNavigationState = (initialState = 0) => {
    const [selectedIndex, setSelectedIndex] = React.useState(initialState);
    return { selectedIndex, onSelect: setSelectedIndex };
};



const BottomNavigator = (props: any) => {

     
    const CartIcon = (props: any) => (
        
        <View>
            <Icon {...props} name='list'/>
        </View>

    );

    const BellIcon = (props: any) => (
        <View>
            <Icon {...props} name='settings-2'/>
        </View>
    );

    const { navigate } = useNavigation();
    const setBottomSelectedIndex = (index: any) => {
       
      switch (index) {
        case 0:
            navigate(Screens.HomeScreen);
        break;
        case 1:
            Alert.alert('On development')
            //navigate(Screens.ShoppingCartScreen);
        break;
        case 2:
            Alert.alert('On development')
        break;
        case 3:
           Alert.alert('On development')
        break;
        case 4:
            navigate('LoginScreen');
        break;
      }

    };


  

    const [selectedIndex, setSelectedIndex] = React.useState(props.index);
    const topState = useBottomNavigationState();




    return (
    <>
        <React.Fragment>
            <BottomNavigation 
            style={{ marginVertical: 15 }}
            {...topState}
            selectedIndex={props.index}
            onSelect={index => { setBottomSelectedIndex(index),  setSelectedIndex(index) }}>
            <BottomNavigationTab icon={HomeIcon}/>
            <BottomNavigationTab icon={CartIcon}/>
            <BottomNavigationTab icon={BoxIcon}/>
            <BottomNavigationTab icon={BellIcon}/>
            <BottomNavigationTab icon={PersonIcon}/>
            </BottomNavigation>
        </React.Fragment>
    </>
    )

}



export default BottomNavigator 