import Axios, { AxiosPromise, AxiosRequestConfig, Method } from 'axios';
import Environment from '../constants/Environment';
import AsyncStorage from '@react-native-community/async-storage';
import Strings from '../constants/Strings';

let axiosInterceptors: number = -1;

function apiHelper(
  method: Method,
  path: string,
  params: { [key: string]: any } = {},
  axiosConfig: AxiosRequestConfig
): AxiosPromise<any> {
  return AsyncStorage.getItem(Strings.KeyStorage.Authorization).then(
    async (AuthorizationToken: any) => {
      let config: AxiosRequestConfig = {
        baseURL: Environment.baseUrl,
        url: path,
      };

      switch (method) {
        case 'post':
          config.headers = {
            'content-type': 'application/json',
            responseType: 'application/json',
          };
          config.data = params;
          break;
        case 'get':
          config.params = params;
          break;
      }

      if (AuthorizationToken) {
        config.headers = {
          ...config.headers,
          Authorization: `Bearer ${AuthorizationToken}`, 
        };
      }
      
      config.headers = {
        ...config.headers,
        'x-seller-id' : 1
      }

      axiosInterceptors = Axios.interceptors.response.use(
        (response: any) => {
          return response && response.data;
        },
        (err: any) => {
          throw err.response;
        }
      );

      if (axiosInterceptors > -1) {
        Axios.interceptors.response.eject(axiosInterceptors);
      }

      return Axios.request({ method: method, ...config, ...axiosConfig });
    }
  );
}

const api = {
  post: (
    path: string,
    params: { [key: string]: any } = {},
    axiosConfig: AxiosRequestConfig = {}
  ) => apiHelper('post', path, params, axiosConfig),
  get: (
    path: string,
    params: { [key: string]: any } = {},
    axiosConfig: AxiosRequestConfig = {}
  ) => apiHelper('get', path, params, axiosConfig),
};

export default api;
