import React, { useState, useEffect } from 'react';
import Styles from './Style';
import { DefaultStyles } from '../../../styles';
import { Image, ScrollView, View, FlatList } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import BottomNavigator from '../../components/BottomNavigator';


import {
    Text,
    Button,
    Layout,
    Input,
    Icon,
    Divider,
    Card,
    TopNavigationAction,
    IndexPath, Select, SelectItem
  } from '@ui-kitten/components';
import AsyncStorage from '@react-native-community/async-storage';

const BackIcon = (style: any) => {
    return (
        <Layout>
            <Icon style={{ width:30, height:30 }} fill='black' name="chevron-left" />
        </Layout>
    );
};

const CartIcon = (props: any) => (
  <Icon {...props} name='shopping-cart'/>
);

const useBottomNavigationState = (initialState = 0) => {
    const [selectedIndex, setSelectedIndex] = React.useState(initialState);
    return { selectedIndex, onSelect: setSelectedIndex };
};


const HomeScreen = ({navigation} : any) =>{

  const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0));
    const [menuVisible, setMenuVisible] = useState(false);
    const [value, setValue] = useState('');
    const [collectionTitle, setCollectionTitle] = useState('')
    const toggleMenu = () => {
      setMenuVisible(!menuVisible);
    };

    const BackAction = (style: any) => (
        <TopNavigationAction icon={BackIcon} onPress={() => navigation.pop()} />
    );
    
    let [productData, setProductData] = useState([]);
    


    const renderOption = (title: any) => (
      <SelectItem title={title}/>
    );
    
    const topState = useBottomNavigationState();
    const bottomState = useBottomNavigationState();
    const renderIcon = (props: any) => (
          <Icon {...props} name={'search'}/>
    );



    return(
        <>
            <Layout style={Styles.Wrapper}>
                <Layout
                 style={{ ...DefaultStyles.TopNavigation }}
                >
                    <Layout style={[Styles.container, {justifyContent:'center', marginBottom:10}]}>
                        <Text style={{ textAlign:'center' }}>PRESS THE SCAN BUTTON TO READ THE RFID</Text>
                    </Layout>
                </Layout>
                <Divider/>
                <KeyboardAwareScrollView keyboardShouldPersistTaps='handled' style={[Styles.KeyBoardScrollView, {paddingLeft:50, paddingRight:50, paddingTop:50}]}>  
                      
                <Card style={{  justifyContent: 'center', alignItems: 'center', height:400, marginTop:40 }}>
                    <Image
                        source={require('../../images/rfid.png')}
                        style={{
                            width: 200,
                            height: 200,
                            tintColor: 'black',
                            alignContent: 'center',
                        }}
                    />
                </Card>

                </KeyboardAwareScrollView>
            <BottomNavigator index={0} />
            </Layout>
        </>
    );
}

export { HomeScreen }