import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
    },
    Wrapper:{
      height: '100%',
    },
    HeaderTextColor: {
      color: '#3a89ee',
      fontWeight: 'bold'
    },
    SubHeaderTextColor: {
      color: '#b6b9c1',
      textAlign:'center',
      marginTop:10,
    },
    image: {
      flex: 1,
      resizeMode: 'contain',
      justifyContent: 'center',
      width: '100%',
      height: 265,
      backgroundColor:'white'
    },
    text: {
      color: 'white',
      fontSize: 42,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    tinyLogo: {
      width: 500,
      height: 500,
    },
    imageLogo:{
      marginTop:40,
      justifyContent: 'center',
      width: '100%',
      height: 85,
      resizeMode: 'contain'
    },
    FormContainerLayout:{
      marginTop:'5%',
      marginLeft:'7%',
      marginRight:'7%',
      alignItems: 'center',
    },
    ContainerChildLayout:{
      flex: 1,
      width: '100%',
      marginTop:15
    },
    InputTopSpacing:{
        marginTop:10
    },
    Label:{
      color:'gray'
    },
    tab: {
      height: 102,
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
    },
    contentContainer: {
      paddingVertical: 20
    },
    logo: {
      width: 50,
      height: 50,
    },
    LogoContainer: {
      flex:1, 
      justifyContent:'center', 
      alignItems:'center'
    },
    SearchContainer:{
      flex:6,
      paddingRight:20
    },
    SearchInput:{
      marginTop:13,
      marginBottom:10,
      marginRight:10,
      marginLeft:10,
      width:'100%'
    },
    KeyBoardScrollView:{
      paddingLeft:10,
      paddingRight:10, 
      paddingTop:10,
      backgroundColor: '#E8E8E8'
    },
    FeaturedProductContainer:{
      borderRadius:10,
      marginTop:10,
      marginBottom:20,
      paddingTop:10,
      backgroundColor:'#F5F5F5',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
    },
    FeaturedProductHeader:{
      padding:10,
      flex: 1,
      flexDirection: 'row',
      borderBottomWidth: 1,  
      borderColor:'#DBDBDB',
    },
    FeaturedProductScrollView:{
      marginTop:15,
    },
});

export default Styles;