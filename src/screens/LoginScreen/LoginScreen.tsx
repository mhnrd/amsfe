import React, { useState, useRef, useEffect } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { ImageBackground, View, TouchableWithoutFeedback, Animated, Alert } from "react-native";
import Styles from './Style';
import { DefaultStyles } from '../../../styles';
//import AccountsService from '../../services/Accounts.services';
//import { setUserAccountData, setUserAuthorizationToken, setUserInformation } from '../utils/Session';
import { NavigationActions, StackActions } from 'react-navigation';
import PhoneInput from "../../lib/phonenumberinput";

import {
  Text,
  Layout,
  Input,
  Button,
  Icon,
  Divider
} from '@ui-kitten/components';

import Loader from '../../components/loader/Loader';
import Toast from 'react-native-simple-toast';

const FacebookIcon = (props: any) => (
  <Icon {...props} name='facebook'/>
);

const EmailIcon = (props: any) => (
  <Icon {...props} name='email'/>
);

console.disableYellowBox = true;

const LoginScreen = ({ navigation } : any) => {

    var [username, setUsername] = useState('');
    var [usernameError, setUsernameError] = useState(false);
    var [usernameErrorMessage, setUsernameErrorMessage] = useState('');

    var [password, setPassword] = useState('');
    var [passwordError, setPasswordError] = useState(false);
    var [passwordErrorMessage, setPasswordErrorMessage] = useState('');



    const [secureTextEntry, setSecureTextEntry] = useState(true);
    const fadeAnim = useRef(new Animated.Value(0)).current;

    const [requestLoaderVisible, setRequestLoaderVisible] = useState(false);
    const [requestLoaderMessage, setRequestLoaderMessage] = useState('');
    const [requestLoaderType, setRequestLoaderType] = useState('');

    var [mobile, setMobileNumber] = useState('');
    var [mobileNumberError, setMobileNumberError] = useState(false);
    var [mobileNumberErrorMessage, setMobileNumberErrorMessage] = useState('');

    var [formattedMobileNumber, setFormattedMobileNumber] = useState('');
    var phoneInput = useRef<PhoneInput>(null);

    
    const toggleSecureEntry = () => {
      setSecureTextEntry(!secureTextEntry);
    };

    const renderIcon = (props: any) => (
      <TouchableWithoutFeedback onPress={toggleSecureEntry}>
        <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
      </TouchableWithoutFeedback>
    );

   const sendLoginRequest = async () => {
    var errorCounter = 0;

    const payload = {
      username: username,
      password: password
    }
    
    console.log(payload)

   
    if(password == ''){
      setPasswordError(true)
      setPasswordErrorMessage('Password is required.')
      errorCounter += 1;
    }
    
    if(username == ''){
      setUsernameError(true)
      setUsernameErrorMessage('Email is required.')
      errorCounter += 1;
    }

    if(errorCounter != 0){

      Toast.show('Please Fill up all the field required.');

    }else{
      navigation.navigate('HomeScreen');
      setRequestLoaderVisible(true)
      setRequestLoaderType('loader')
      setRequestLoaderMessage('Logging in...');
      setRequestLoaderVisible(false)
  
    }
   
    
   }

    const getLoginLogo = () => {
      return require('../../images/logo.jpg');
    };
   
    useEffect(() => {
      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 10000,
        useNativeDriver: true
      }).start();
    }, []);
  
    return(
      <>
        <Loader 
          type={requestLoaderType}
          title={requestLoaderMessage} 
          visible={requestLoaderVisible}
          color={'#3a89ee'}
        />
        <Layout style={Styles.Wrapper}>
          <KeyboardAwareScrollView>
              <Animated.Image 
                source={ getLoginLogo() } 
                style={ Styles.imageLogo}
              />
              <Layout style={Styles.FormContainerLayout}>
                  <Text 
                  category='h4' 
                  style={Styles.HeaderTextColor}
                  >
                    LOGIN
                  </Text>
                  <Text 
                    category='s2' 
                    style={Styles.SubHeaderTextColor}
                  >
                    Login your account to Asset Management System.
                  </Text>
                  <View 
                    style={Styles.ContainerChildLayout}
                  >
                      <Layout style={{ marginTop:10 }}>
                        <Text style={Styles.Label} category='label'>Email</Text>
                        <Input
                          placeholder='Type your email'
                          value={username}
                          status = {usernameError ? 'danger' : ''}
                          caption = {usernameError ? usernameErrorMessage : ''}
                          onChangeText={nextValue => { setUsername(nextValue), nextValue == '' ? setUsernameError(true) : setUsernameError(false)}}
                          style={DefaultStyles.DefaultInputTopSpacing}
                        />

                    </Layout>
                   
                    <Layout style={Styles.InputTopSpacing}>
                      <Text style={Styles.Label} category='label'>Password</Text>
                    <Input
                      value={password}
                      placeholder='Type your password'
                      accessoryRight={renderIcon}
                      secureTextEntry={secureTextEntry}
                      status = {passwordError ? 'danger' : ''}
                      caption = {passwordError ? passwordErrorMessage : ''}
                      onChangeText={nextValue => { setPassword(nextValue), nextValue == '' ? setPasswordError(true) : setPasswordError(false) }}
                      style={DefaultStyles.DefaultInputTopSpacing}
                    />
                    </Layout>
                    <Button 
                      style={DefaultStyles.DefaultButtonSize} 
                      size='medium'
                      onPress={() =>  sendLoginRequest()}
                    >
                      LOGIN
                    </Button>

                  </View>
                  <View 
                    style={Styles.ContainerChildLayout}
                  >
                 
                  
                  </View>
              </Layout>
          </KeyboardAwareScrollView>
        </Layout>
      </>
    );
    
};

export { LoginScreen };