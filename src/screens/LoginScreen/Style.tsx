import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "column",
      backgroundColor:'white'
    },
    Wrapper:{
      height: '100%',
    },
    HeaderTextColor: {
      color: '#000080',
      fontWeight: 'bold'
    },
    SubHeaderTextColor: {
      color: '#b6b9c1',
      textAlign:'center',
      marginTop:1,
      marginRight:5,
    },
    BreakLineHeaderTextColor: {
      color: '#b6b9c1',
      textAlign:'center',
      marginTop:-10,
      marginLeft:10,
      marginRight:10
    },
    image: {
      flex: 1,
      resizeMode: 'contain',
      justifyContent: 'center',
      width: '100%',
      height: 265,
      backgroundColor:'white',
      marginTop:-40
    },
    text: {
      color: 'white',
      fontSize: 42,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    tinyLogo: {
      width: 500,
      height: 500,
    },
    imageLogo:{
      marginTop:60,
      justifyContent: 'center',
      width: '100%',
      height: 225,
      resizeMode: 'contain'
    },
    FormContainerLayout:{
      marginTop:'5%',
      marginLeft:'7%',
      marginRight:'7%',
      alignItems: 'center',
    },
    ContainerChildLayout:{
      flex: 1,
      width: '100%',
      marginTop:15
    },
    InputTopSpacing:{
      marginTop:10
    },
    Label:{
      color:'gray'
    }
});

export default Styles;