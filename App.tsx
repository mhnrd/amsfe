import React , { useState } from 'react';
import {
  dark as darkTheme,
  light as lightTheme,
  mapping,
} from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { RoutedApp } from './src/components/StackNavigator';

const App = () => {
  
  const [ready, setReady] = useState(false);
  const [theme, setTheme] = useState(lightTheme);
  const [selectedTheme, setSelectedTheme] = useState(null);

  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider mapping={mapping} theme={{ ...theme }}>
        <RoutedApp />
      </ApplicationProvider>
    </>
  )

}

export default App;